
# Getting started

To run the examples, from the commandline execute:

```
npm install
npm run start:dev
```

You can now open the browser at `http://localhost:8080` to open the demo.

## Examples

**hello-world** Small React component

**hello-state** Small React component with some state

**input-demo** Simple demo with a form component

**todo-app-final** Small app containing ToDo's.