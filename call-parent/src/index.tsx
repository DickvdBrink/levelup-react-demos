import * as React from "react";
import * as ReactDOM from "react-dom";

interface MyCounterState { counter: number; }

class App extends React.Component<{}, MyCounterState> {
  constructor(props: any) {
    super(props);
    this.state = {counter: 0};
  }

  resetClick = () => {
    this.setState({
      counter: 0
    });
  }

  increment = () => {
    this.setState((previousState, currentProps) => {
      return { counter: previousState.counter + 1 };
    });
  }

  render() {
    return (
      <React.Fragment>
        <MyLabel text={this.state.counter.toString()} />
        <MyButton handleClick={this.resetClick} text="Reset" />
        <MyButton handleClick={this.increment} text="+" />
      </React.Fragment>
    );
  }
}

interface MyLabelProps {
  text: string
}
class MyLabel extends React.Component<MyLabelProps, {}> {
  componentWillReceiveProps(nextProps: Readonly<MyLabelProps>) {
    console.log("Component got new props", nextProps);
  }
  render() {
    console.log("Render MyLabel");
    return <p>{this.props.text}</p>;
  }
}

interface MyButtonProps {
  handleClick: () => void;
  text: string;
}
class MyButton extends React.Component<MyButtonProps, {}> {

  /*shouldComponentUpdate() {
    return false;
  }*/

  render() {
    console.log("Render MyButton");
    return <button onClick={this.props.handleClick}>{this.props.text}</button>;
  }
}

ReactDOM.render(
  <App />,
  document.getElementById("container")
);
