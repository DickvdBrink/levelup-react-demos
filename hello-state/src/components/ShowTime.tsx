import * as React from "react";
import * as ReactDOM from "react-dom";

export interface ShowTimeProps { city: string; }
export interface ShowTimeState { date: Date; }

export default class ShowTime extends React.Component<ShowTimeProps, ShowTimeState> {
  private timer: number;

  constructor(props: ShowTimeProps)
  {
    super(props)
    this.state = {date: new Date()};
  }

  render() {
    return <div>
      <h1>Hello from {this.props.city}!</h1>
      <p>The current time is: {this.state.date.toLocaleString()}</p>
    </div>;
  }

  componentDidMount() {
    this.timer = window.setInterval(() => {
      // Wrong!
      //this.state.date = new Date();

      this.setState({
        date: new Date()
      });
    }, 1000);
  }

  componentWillUnmount() {
    window.clearInterval(this.timer);
  }
}
