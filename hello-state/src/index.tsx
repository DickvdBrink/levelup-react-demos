import * as React from "react";
import * as ReactDOM from "react-dom";

import ShowTime from "./components/ShowTime";

class App extends React.Component {

  render() {
    return <ShowTime city="Apeldoorn" />;
  }
}

ReactDOM.render(
  <App />,
  document.getElementById("container")
);

