import * as React from "react";
import * as ReactDOM from "react-dom";

interface SimpleFormState { name?: string; city?: string; }

class SimpleForm extends React.Component<{}, SimpleFormState> {
  constructor(props: any) {
    super(props);
    this.state = {name: '', city: ''};
  }

  handleChange = (event: React.FormEvent<HTMLInputElement>) => {
    if (event.currentTarget.value.length < 10)
    {
      this.setState({
        [event.currentTarget.name]: event.currentTarget.value
      });
    }
  }

  handleSubmit = (event: React.FormEvent<any>) => {
    alert('A name was submitted: ' + this.state.name);
    event.preventDefault();
  }

  render() {
    let element: JSX.Element;
    if (this.state.name && this.state.city) {
      element = <h1>Hello {this.state.name}! Welcome to {this.state.city}</h1>;
    } else {
      element = <p>Please fill in the form</p>;
    }

    return (
      <form onSubmit={this.handleSubmit}>
        <label>
          Name:
          <input type="text" value={this.state.name} onChange={this.handleChange} name="name" />
        </label>
        <label>
          City:
          <input type="text" value={this.state.city} onChange={this.handleChange} name="city"/>
        </label>
        <input type="submit" value="Submit" />
        {element}
      </form>
    );
  }
}


ReactDOM.render(
  <SimpleForm />,
  document.getElementById("container")
);
