import * as React from "react";
import * as ReactDOM from "react-dom";

export interface HelloProps { city: string; }

class Hello extends React.Component<HelloProps, {}> {

    render() {
        return <h1>Hello from {this.props.city}!</h1>;
    }
}

ReactDOM.render(
    <Hello city="Apeldoorn" />,
    document.getElementById("container")
);
