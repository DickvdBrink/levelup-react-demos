import * as React from "react";
import * as ReactDOM from "react-dom";

interface TodoProp {
  todo: ToDoItem;
  remove: (item: ToDoItem) => void;
  done: (item: ToDoItem) => void;
}

export class Todo extends React.Component<TodoProp, {}> {

  handleDeleteClick = (event: any) =>
  {
    this.props.remove(this.props.todo);
  }

  handleDoneClick = (event: any) =>
  {
    this.props.done(this.props.todo);
  }

  render() {
    return (<div>
      <span style={{ textDecoration: this.props.todo.done ? 'line-through' : ''}}>
        {this.props.todo.text}</span>
      <button className="btn btn-primary m-1" onClick={this.handleDoneClick}>Done</button>
      <button className="btn btn-danger m-1" onClick={this.handleDeleteClick}>Delete</button>
    </div>);
  }
}
