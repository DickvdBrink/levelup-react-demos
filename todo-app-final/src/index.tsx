import * as React from "react";
import * as ReactDOM from "react-dom";

import { TodoForm } from "./components/todo-form";
import { TodoList } from "./components/todo-list";

interface ToDoAppState {
  data: ToDoItem[];
}

class ToDoApp extends React.Component<{}, ToDoAppState> {

  constructor(props: any) {
    super(props);
    this.state = {
      data: []
    }
  }

  addTodo = (todo: ToDoItem) => {
    this.state.data.push(todo);
    this.setState({data: this.state.data});
  }

  handleRemove = (todo: ToDoItem) => {
    const index = this.state.data.indexOf(todo);
    if (index >= 0)
    {
      this.state.data.splice(index, 1);
      this.setState({data: this.state.data});
    }
  }

  handleDone = (todo: ToDoItem) => {
    todo.done = !todo.done
    this.setState({data: this.state.data});
  }

  render() {
    return (
      <div className="row justify-content-center">
        <div className="col-md-6">
          <h1>Todo list ({this.state.data.length})</h1>
          <TodoForm addTodo={this.addTodo}/>
          <TodoList 
            todos={this.state.data} 
            remove={this.handleRemove}
            done={this.handleDone}
          />
        </div>
      </div>
    );
  }
}


ReactDOM.render(
  <ToDoApp />,
  document.getElementById("container")
);
