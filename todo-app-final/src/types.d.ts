interface ToDoItem {
  text: string;
  done: boolean;
  dateAdded: Date;
}
